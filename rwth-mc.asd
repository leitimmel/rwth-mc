;;;; rwth-mc.asd
;;;; This file is released under the terms of the ISC license. Read LICENSE or
;;;; <https://opensource.org/licenses/ISC> for more information.

(asdf:defsystem #:rwth-mc
  :description "A collection of simple MC question solvers"
  :author "Alex Immel <alex.immel@outlook.com>"
  :license "ISC"
  :serial t
  :components ((:file "package")
               (:file "rwth-mc")))

