;;;; rwth-mc.lisp
;;;; This file is released under the terms of the ISC license. Read LICENSE or
;;;; <https://opensource.org/licenses/ISC> for more information.

(in-package #:rwth-mc)

(defun dim (trans exp dim)
  (let ((p 1)
        (q 1))
    (loop for i from 0 to (1- dim) do
      (setf p (* p (- (expt trans exp) (expt trans i))))
      (setf q (* q (- (expt trans dim) (expt trans i)))))
    (/ p q)))

(defun inv-gln (trans format)
  (let ((res 1))
    (loop for i from 0 to (1- format) do
      (setf res (* res (- (expt trans format) (expt trans i)))))
    res))

(defun notinv-gln (trans format)
  (- (expt trans (expt format 2)) (inv-gln trans format)))

(defun hom (trans exp1 exp2)
  (expt trans (* exp1 exp2)))

(defun sol-rk (trans col rk)
  (expt trans (- col rk)))

(defun rk-count-old (trans lin col rk)
  (let ((res 1)
        (top (min lin col)))
    (loop for i from 0 to (1- top) do
      (setf res (* res (- (expt trans top) (expt trans i)))))
    (* res (expt (expt trans top) (- (max lin col) top)))))

(defun rk-count (trans lin col)
  (let ((res 1))
    (loop for i from 0 to (1- (min lin col))
          do (setf res (* res (- (expt trans (max lin col)) (expt trans i)))))
    res))
