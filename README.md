# RWTH LA MC solvers

## Wie führt man die Skripte aus?

Man benötigt einen Common Lisp-Interpreter:

OS           | empfohlen
-------------|----------
Windows      | Gott gnade deiner Seele. Außerdem SBCL, CLISP.
Linux/Unix   | SBCL, CLISP
macOS        | SBCL, CCL

* [SBCL](https://www.sbcl.org)
* [CLISP](https://clisp.sourceforge.net) (einsteigerfreundlicher als SBCL)
* [CCL](https://ccl.clozure.com) (kompiliert schneller als SBCL,
    führt langsamer aus)

### SBCL

```bash
sbcl --load package.lisp --load rwth-mc.lisp
```

### CLISP

CLISP kann meines Wissens nach nicht mehr als eine Datei von der Kommandozeile
laden. Man muss also eine einzelne Datei daraus machen:

```bash
cat package.lisp rwth-mc.lisp > clisp-file.lisp
```

Diese kann man dann mit CLISP laden:

```bash
clisp -repl clisp-file.lisp
```

<!--
```bash
clisp -repl package.lisp rwth-mc.lisp
```
-->

### CCL

```bash
ccl --load package.lisp --load rwth-mc.lisp
```

Jetzt befindet man sich im interaktiven LISP-Interpreter und alle Funktionen
dieser Package stehen zur Verfügung. Ein beispielhafter Aufruf wäre:

```lisp
(rwth-mc:dim 4 7 4)
```

## dim

Löst Aufgaben vom Typ `{U≤F₃⁸|dim_F₃ U = 3}`

**dim** *trans* *exp* *dim* => *int*

* *trans* -- Basis des Primkörpers
* *exp* -- Exponent des Körpers
* *dim* -- gesuchte Dimension

## inv-gln

Löst Aufgaben vom Typ `{A ∈0F₂^(3×3)|A ist invertierbar}`

**inv-gln** *trans* *format* => *int*

* *trans* -- Basis des Primkörpers
* *format* -- Dimension der Matrix (z.B. bei 4×4-Matrix: format = 4)

## notinv-gln

Löst Aufgaben vom Typ `{A ∈0F₂^(3×3)|A ist nicht invertierbar}`

**inv-gln** *trans* *format* => *int*

* *trans* -- Basis des Primkörpers
* *format* -- Dimension der Matrix (z.B. bei 4×4-Matrix: format = 4)

## hom

Löst Aufgaben vom Typ `Hom_F₇(F₇⁷,F₇⁵)`

**hom** *trans* *exp1* *exp2* => *int*

* *trans* -- Basis des Primkörpers
* *exp1* -- Exponent des ersten Körpers
* *exp2* -- Exponent des zweiten Körpers

## sol-rk

Löst Aufgaben vom Typ `Sol(A,0) für ein A ∈ F₇^(7×6) mit rk_F₇ A = 3`

**sol-rk** *trans* *col* *rk* => *int*

* *trans* -- Basis des Primkörpers
* *col* -- Anzahl der Spalten des Körpers
* *rk* -- Rang der Matrix

## rk-count

Löst Aufgaben vom Typ `{A ∈ F₁₃^(3×7)|rk_F₁₃ A = 3}`

**rk-count** *trans* *lin* *col* => *int*

* *trans* -- Basis des Primkörpers
* *lin* -- Anzahl der Zeilen der Matrix
* *col* -- Anzahl der Spalten der Matrix
