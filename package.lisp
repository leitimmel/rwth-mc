;;;; package.lisp
;;;; This file is released under the terms of the ISC license. Read LICENSE or
;;;; <https://opensource.org/licenses/ISC> for more information.

(defpackage #:rwth-mc
  (:use #:cl)
  (:export dim inv-gln notinv-gln hom sol-rk rk-count))
